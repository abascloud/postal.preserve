image: node:18

stepdefinitions:
  - build: &build
      name: Install & Build
      script:
        - sh setupNPM.sh ${NPM_ABAS_TOKEN}
        - npm install
        - npm run build
      artifacts:
        - lib/**
        - package.json
        - package-lock.json
        - node_modules/**

  - publish: &publish
      name: Publish artifact
      script:
        - apt update && apt install -y jq
        - sh setupNPM.sh ${NPM_ABAS_TOKEN}
        - VERSION=$(echo ${BITBUCKET_TAG} | sed 's/^[^0-9]*//')
        - PACKAGE="$(jq '.version = "'"${VERSION}"'"' package.json)" && echo "${PACKAGE}" > package.json
        - npm publish --verbose
      artifacts:
        - package.json

  - step: &npm-install-owasp
      name: "npm install"
      script:
        - sh setupNPM.sh ${NPM_ABAS_TOKEN}
        - npm install
      artifacts:
        - node_modules/**
        - package.json
        - package-lock.json

  - step: &owasp-deploy-deps
      name: "deploy deps to artifactory for future scanning"
      image: alpine:latest
      script:
        - apk add curl zip
        - if [[ "${BITBUCKET_TAG}" == "" ]]; then echo "This build has no version / did not originate from a bitbucket tag. Skipping archiving deps."; exit 0; fi;
        - export FILENAME=${BITBUCKET_REPO_SLUG}-${BITBUCKET_TAG}_deps-${BITBUCKET_BUILD_NUMBER}
        - echo "build $FILENAME"
        - mkdir deps
        - mv node_modules deps/node_modules
        - cp package.json deps
        - cp package-lock.json deps
        - cd deps
        - zip -qr ../${FILENAME}.zip .
        - cd ..
        # upload new deps package to artifactory
        - >
          curl -X PUT --fail-with-body \
            --user ${OWASP_ARTIFACTORY_USER}:${OWASP_ARTIFACTORY_PASSWORD} \
            --upload-file ${FILENAME}.zip ${OWASP_ARTIFACTORY_REPO}/postal/${FILENAME}.zip
        # set repo variable to new deps package
        - >
          curl -X PUT --fail-with-body \
            -u "${OWASP_BITBUCKET_USER}:${OWASP_BITBUCKET_PASSWORD}" \
            --url "https://api.bitbucket.org/2.0/repositories/abascloud/${BITBUCKET_REPO_SLUG}/pipelines_config/variables/%7B${OWASP_BITBUCKET_DEPS_PROD_UUID}%7D" \
            -H 'Content-Type:application/json' \
            -d "{\"key\":\"OWASP_CURRENT_DEPS_PROD\",\"value\":\"${FILENAME}\"}"
        # remove predecessor deps package and report folder from artifactory ($OWASP_CURRENT_DEPS_PROD still has the old value)
        - >
          if [ "$OWASP_CURRENT_DEPS_PROD" != "null" ];
          then
            curl -X DELETE --fail-with-body \
              --user ${OWASP_ARTIFACTORY_USER}:${OWASP_ARTIFACTORY_PASSWORD} \
              ${OWASP_ARTIFACTORY_REPO}/postal/${OWASP_CURRENT_DEPS_PROD}.zip
            curl -X DELETE \
              --user ${OWASP_ARTIFACTORY_USER}:${OWASP_ARTIFACTORY_PASSWORD} \
              ${OWASP_ARTIFACTORY_REPO}/postal/${OWASP_CURRENT_DEPS_PROD}
          fi

  - step: &owasp-deploy-report
      name: "deploy report to artifactory"
      image: alpine:latest
      script:
        - apk add curl zip
        - export FILENAME=$(date +"%Y_%m_%d-%H_%M")
        - cd security-report
        - zip -qr ../${FILENAME}.zip .
        - cd ..
        - >
          curl -X PUT --fail-with-body \
            --user ${OWASP_ARTIFACTORY_USER}:${OWASP_ARTIFACTORY_PASSWORD} \
            --upload-file ${FILENAME}.zip ${OWASP_ARTIFACTORY_REPO}/postal/${OWASP_CURRENT_DEPS_PROD}/${FILENAME}.zip

  - step: &owasp-get-current-prod-deps
      name: "getting deps from artifactory"
      image: alpine:latest
      script:
        - if [ "${OWASP_CURRENT_DEPS_PROD}" == "null" ]; then echo "No deps set for scanning. Nothing to do."; exit 1; fi
        - apk add curl unzip
        - echo "Getting deps for build ${OWASP_CURRENT_DEPS_PROD}"
        - >
          curl --fail-with-body --location \
            --user "${OWASP_ARTIFACTORY_USER}:${OWASP_ARTIFACTORY_PASSWORD}" \
            ${OWASP_ARTIFACTORY_REPO}/postal/${OWASP_CURRENT_DEPS_PROD}.zip -o deps.zip
        - unzip -qo deps.zip
      artifacts:
        - node_modules/**
        - package.json
        - package-lock.json

  - step: &owasp-security-scan
      name: "scan for security vulnerabilities"
      services:
        - docker
      script:
        - docker run --rm -v /opt/atlassian/bitbucketci/agent/build/:/src:z -v /opt/atlassian/bitbucketci/agent/build/security-report:/report:z owasp/dependency-check-action:latest --scan package-lock.json -f HTML -f JSON -f CSV --project "${BITBUCKET_REPO_SLUG}" --out /report
        - mv /opt/atlassian/bitbucketci/agent/build/security-report/dependency-check-report.json /opt/atlassian/bitbucketci/agent/build/security-report/dependency-check-report_full.json
        - mv /opt/atlassian/bitbucketci/agent/build/security-report/dependency-check-report.csv /opt/atlassian/bitbucketci/agent/build/security-report/dependency-check-report_full.csv
        - mv /opt/atlassian/bitbucketci/agent/build/security-report/dependency-check-report.html /opt/atlassian/bitbucketci/agent/build/security-report/dependency-check-report_full.html
        - docker run --rm -v /opt/atlassian/bitbucketci/agent/build/:/src:z -v /opt/atlassian/bitbucketci/agent/build/security-report:/report:z owasp/dependency-check-action:latest --scan package-lock.json -f HTML -f JSON -f CSV --project "${BITBUCKET_REPO_SLUG}" --nodeAuditSkipDevDependencies --nodePackageSkipDevDependencies --out /report
        - mv /opt/atlassian/bitbucketci/agent/build/security-report/dependency-check-report.json /opt/atlassian/bitbucketci/agent/build/security-report/dependency-check-report_without-dev.json
        - mv /opt/atlassian/bitbucketci/agent/build/security-report/dependency-check-report.csv /opt/atlassian/bitbucketci/agent/build/security-report/dependency-check-report_without-dev.csv
        - mv /opt/atlassian/bitbucketci/agent/build/security-report/dependency-check-report.html /opt/atlassian/bitbucketci/agent/build/security-report/dependency-check-report_without-dev.html
      artifacts:
        - security-report/**

  - step: &owasp-add-metadata
      name: "combine and add metadata"
      services:
        - docker
      script:
        - chmod 777 /opt/atlassian/bitbucketci/agent/build/security-report/
        - chmod -R 666 /opt/atlassian/bitbucketci/agent/build/security-report/*
        - docker login -u ${DOCKER_USER} -p ${DOCKER_PASSWORD} ${ARTIFACTORY_INTERNAL_DOMAIN}
        - CONTAINER_ID=$(docker run -it -d -v $(pwd):/opt/atlassian/bitbucketci/agent/build ${ARTIFACTORY_INTERNAL_DOMAIN}/intra/owasp-report-add-dev-dep-metadata:latest)
        - docker exec $CONTAINER_ID python3 /opt/owasp-report-add-dev-dep-metadata.py
      artifacts:
        - security-report/**

pipelines:
  tags:
    '*':
      - step: *build
      - step: *publish
      - step: *owasp-deploy-deps

  custom:
    owaspScan:
      - step: *npm-install-owasp
      - step: *owasp-security-scan

    owaspScanProdDeps:
      - step: *owasp-get-current-prod-deps
      - step: *owasp-security-scan
      - step: *owasp-add-metadata
      - step: *owasp-deploy-report
