/*jshint esversion: 6 */

const gulp = require("gulp");
const fileImports = require("gulp-imports");
const header = require("gulp-header");
const beautify = require("gulp-beautify");
const hintNot = require("gulp-hint-not");
const terser = require("gulp-terser");
const rename = require("gulp-rename");
const plato = require("gulp-plato");
const gutil = require("gulp-util");
const express = require("express");
const path = require("path");
const pkg = require("./package.json");
const open = require("open");
const port = 3080;

var banner = ["/**",
    " * <%= pkg.name %> - <%= pkg.description %>",
    " * Author: <%= pkg.author %>",
    " * Version: v<%= pkg.version %>",
    " * Url: <%= pkg.homepage %>",
    " * License(s): <% pkg.licenses.forEach(function( license, idx ){ %><%= license.type %><% if(idx !== pkg.licenses.length-1) { %>, <% } %><% }); %>",
    " */",
    ""
].join("\n");

gulp.task("combine", function() {
    return gulp.src(["./src/postal.preserve.js"])
        .pipe(header(banner, {
            pkg: pkg
        }))
        .pipe(fileImports())
        .pipe(hintNot())
        .pipe(beautify({
            indentSize: 4,
            preserveNewlines: false
        }))
        .pipe(gulp.dest("./lib/"))
        .pipe(terser())
        .pipe(header(banner, {
            pkg: pkg
        }))
        .pipe(rename("postal.preserve.min.js"))
        .pipe(gulp.dest("./lib/"));
});

gulp.task("default", ["combine"]);

gulp.task("report", function() {
    return gulp.src("./lib/postal.preserve.js")
        .pipe(plato("report"));
});

var createServer = function(port) {
    var p = path.resolve("./");
    var app = express();
    app.use(express.static(p));
    app.listen(port, function() {
        gutil.log("Listening on", port);
    });

    return {
        app: app
    };
};

var servers;

gulp.task("server", ["combine", "report"], function() {
    if (!servers) {
        servers = createServer(port);
    }
    open("http://localhost:" + port + "/index.html");
});
