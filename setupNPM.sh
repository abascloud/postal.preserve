#!/bin/sh

# Setting up npm to use abas registry instead of the public one
# $1 is your token (see artifactory for details)
echo "@abas:registry=https://abasartifactory.jfrog.io/artifactory/api/npm/abas.npm/" > ~/.npmrc
echo "//abasartifactory.jfrog.io/artifactory/api/npm/abas.npm/:_authToken=$1" >> ~/.npmrc
echo "//abasartifactory.jfrog.io/artifactory/api/npm/abas.npm/:email = workflow@abas.de" >> ~/.npmrc
echo "//abasartifactory.jfrog.io/artifactory/api/npm/abas.npm/:always-auth=true" >> ~/.npmrc
echo "progress=false" >> ~/.npmrc
echo "registry=https://registry.abas.sh/artifactory/api/npm/abas.npm.group/" >> ~/.npmrc
