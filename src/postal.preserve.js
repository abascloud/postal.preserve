/*jshint esversion: 6 */
/**
 * postal.preserve - Add-on for postal.js that provides message durability features.
 * Author: Jim Cowart (http://freshbrewedcode.com/jimcowart)
 * Version: v0.2.1
 * Url: http://github.com/postaljs/postal.preserve
 * License(s): MIT
 */
(function(root, factory) {

    if (typeof define === "function" && define.amd) {
      // AMD. Register as an anonymous module.
      define(["lodash", "conduitjs", "postal"], function(_, Conduit, postal) {
        return factory(_, Conduit, postal, root);
      });

    } else if (typeof module === "object" && module.exports) {
      // Node, or CommonJS-Like environments
      module.exports = function(postal) {
        factory(require("lodash"), require("conduitjs"), postal, this);
      };

    } else {
      // Browser globals
      root.postal = factory(root._, root.Conduit, root.postal, root);
    }
  })

  (this, function(_, Conduit, postal, global, undefined) {

    let plugin = (postal.preserve = {
      store: {},
      expiring: []
    });

    postal.channel(postal.configuration.SYSTEM_CHANNEL);

    let dtSort = function(a, b) {
      return b.expires - a.expires;
    };

    postal.addWireTap(function(d, event) {
      let channel = event.channel;
      let topic = event.topic;

      if (event.headers) {
        plugin.store[channel] = plugin.store[channel] || {};
        plugin.store[channel][topic] = plugin.store[channel][topic] || [];

        if (event.headers.preserve) {
          plugin.store[channel][topic].push(event);

        } else if (event.headers.preserveLast) {
          plugin.store[channel][topic] = [ event ];
        }

        // a bit harder to read, but trying to make
        // traversing expired messages faster than
        // iterating the store object's multiple arrays
        if (event.headers.expires && (event.headers.preserveLast || event.headers.preserve )) {
          plugin.expiring.push({
            expires: event.headers.expires,
            purge: function() {
              plugin.store[channel][topic] = _.without(
                plugin.store[channel][topic],
                event
              );
              plugin.expiring = _.without(plugin.expiring, this);
            }
          });
          plugin.expiring.sort(dtSort);
        }
      }
    });

    function purgeExpired() {
      let dt = new Date();
      let expired = _.filter(plugin.expiring, function(x) {
        return x.expires < dt;
      });
      while (expired.length) {
        expired.pop().purge();
      }
    }

    if (!postal.subscribe.after) {
      let orig = postal.subscribe;
      postal.subscribe = new Conduit.Sync({
        context: postal,
        target: orig
      });
    }

    postal.SubscriptionDefinition.prototype.enlistPreserved = function() {
      let channel = this.channel;
      let binding = this.topic;
      let self = this;
      purgeExpired(true);

      if (plugin.store[channel]) {

        _.each(plugin.store[channel], function(msgs, topic) {
          if (postal.configuration.resolver.compare(binding, topic)) {
            _.each(msgs, function(env) {
              self.callback.call(
                self.context ||
                  (self.callback.context && self.callback.context()) ||
                  this,
                env.data,
                env
              );
            });
          }
        });
      }
      return this;
    };

    return postal;
  });
